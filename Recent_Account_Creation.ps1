﻿# This will output user account creation dates for various time frames
#
# By: Landon Lengyel
# Current version: 2019-07-30




Param (
    [Parameter(
        ValueFromPipeline = $true
    )]
    [String]$OrganizationalUnit = $null
)

function Show-Menu 
{
    clear

    Write-Host ""
    Write-Host ""
    Write-Host "=== Enter the time frame you wish to verify ==="
    Write-Host ""
    Write-Host "1: 7 days"
    Write-Host "2: 1 Month (30 Days)"
    Write-Host "3: 2 Months (60 Days)"
    Write-Host "4: 6 Months (180 Days)"
    Write-Host "5: 1 Year (365 Days)"
    Write-Host "6: All time"
    Write-Host "* QUIT"
    Write-Host ""

    $value = Read-Host "Option"
    clear
    return $value
}

function Gather-User-Data ($OrganizationalUnit)
{
    Write-Host "Gathering data, please wait..."
    try { 
        $Users = Get-ADUser -Filter * -SearchBase $OrganizationalUnit -Properties * 
        return $Users
    }
    catch { 
        Write-Host "An error occurred gathering data, and the script must exit - Try restarting PowerShell"
        pause
        exit
    }
}

# $TimeFrame is number of days since account creation
function Account-Creation-Dates ($OrganizationalUnit, $TimeFrame)
{
    clear
    
    # will contain all users that have been created within the $TimeFrame
    [System.Collections.ArrayList]$RelevantUsers = @()

    $Users = Gather-User-Data($OrganizationalUnit)

    ForEach($CurrentUser in $Users){
        Write-Host ("Checking: " + $CurrentUser.name)

        $TimeSpan = New-TimeSpan -Start $CurrentUser.whenCreated -End (Get-Date).ToString()

        # $CurrentUser was created within $TimeFrame
        if ($TimeSpan.Days -le $TimeFrame){
            $RelevantUsers.Add($CurrentUser)
        }
        # $CurrentUser was not created within the #TimeFrame
        else {
            continue
        }
    }

    Write-Host "Data gathering complete"

    clear

    $CsvRequest = Read-Host 'Would you like to export data to a csv file? (Yes/No)'
    if ($CsvRequest -eq 'y' -or $CsvRequest -eq 'yes'){

        try { 
            $CsvPath = Read-Host 'Enter the path to save the csv file (including file name!)'
            New-Item $CsvPath 
        }
        catch { Write-Host 'Unknown error with supplied path'; pause; exit }

        ForEach($CurrentUser in $RelevantUsers) {
            Add-content $CsvPath -value ($CurrentUser.sAMAccountName + "`t" + $CurrentUser.WhenCreated)
        }
    }


    # output desired information
    ForEach($CurrentUser in $RelevantUsers) {
        Write-Host $CurrentUser.sAMAccountName "`t" $CurrentUser.WhenCreated
    }
}

############################################ "Main" ############################################


Import-Module activeDirectory

Write-Host "--- Welcome to the EAW Account Creation Finder ---"


# Retrieve the OU to check (if not passed in a parameter or pipe)
if ($OrganizationalUnit -eq '' -or $OrganizationUnit -eq $null){
    $OrganizationalUnit = Read-Host "Please enter the distinguished name of the OU you wish to check (this can be found in the OU's Attribute Editor)"
}


# Give users options
switch(Show-Menu){
    '1'{ Account-Creation-Dates $OrganizationalUnit 7 }
    '2'{ Account-Creation-Dates $OrganizationalUnit 30 }
    '3'{ Account-Creation-Dates $OrganizationalUnit 60 }
    '4'{ Account-Creation-Dates $OrganizationalUnit 180 }
    '5'{ Account-Creation-Dates $OrganizationalUnit 365 }
    '6'{ Account-Creation-Dates $OrganizationalUnit 328725 }       # It's 900 years. If anyone is still using this in 900 years then that's their problem
    'quit'{ exit }
}

# pause at end for user
pause